// "using" is a keyword for declaring the namespace
// most lines end with ;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;  

public class Player : MonoBehaviour
{
    [SerializeField] private Transform GroundCheckTransform = null;
    [SerializeField] private LayerMask PlayerMask;
    private bool jumpKeyWasPressed;
    private float horizontalInput;
    private Rigidbody rigidbodyComponent;
    private bool isGrounded; // check if the object is grounded
    private int superJumpRemaining;


    // Start is called before the first frame update
    void Start()
    {
        rigidbodyComponent = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpKeyWasPressed = true;
        }

        horizontalInput = Input.GetAxis("Horizontal");
    }

    // is called once every physics update
    private void FixedUpdate()
    {
        rigidbodyComponent.velocity = new Vector3(horizontalInput, rigidbodyComponent.velocity.y, 0);

        //// Ground check transoform is not colliding with anything then the lenght is  zero, if it collide with itself it will be 1 and if it does collide with iteself and the ground it is 2
        //if(Physics.OverlapSphere(GroundCheckTransform.position, 0.1f).Length == 1)
        //{
        //    return;
        //}
        // since we implemented a layer, we don't use lenght anymore, instead we use player mask
        if (Physics.OverlapSphere(GroundCheckTransform.position, 0.1f, PlayerMask).Length == 0)
        {
            return;
        }
        // check if space key is pressed down
        if (jumpKeyWasPressed)
        {
            float jumpPower = 5f;
            if(superJumpRemaining > 0 )
            {
                jumpPower *= 2;
                superJumpRemaining--;
            }
            // check to see if the code works
            //Debug.Log("Space key is pressed down");
            rigidbodyComponent.AddForce(Vector3.up * jumpPower, ForceMode.VelocityChange);
            jumpKeyWasPressed = false;
        }
        // make horizontal movement by setting forward to 0 and vertical to the componenet velocity.y


    }

    private void OnCollisionEnter(Collision collision)
    {
        isGrounded = true;
        // capture relative velocity at the time of collision
        print(collision.relativeVelocity);

    }
    private void OnCollisionExit(Collision collision)
    {
        isGrounded = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 9 )
        {
            Destroy(other.gameObject);
            superJumpRemaining++;
        }
    }

}

